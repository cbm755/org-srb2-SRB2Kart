# org.srb2.SRB2Kart: Flatpak support

This a Flatpak manifest for the racing game Sonic Robo Blast 2 Kart.


## Credits

Based on the rpm spec file of Jan200101 [1]

[1] https://copr.fedorainfracloud.org/coprs/sentry/srb2kart
